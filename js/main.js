'use strict'
let statList = ["Charisma", "Constitution", "Dexterity", "Intellect", "Strength", "Wisdom"];
let statAbr = ["cha", "con", "dex", "int", "str", "wis"]
let classT = ["Barbarian", "Fighter", "Hunter", "Paladin", "Rogue", "Sorcerer", "Warlock", "Wizard"]
let raceT = ["Human", "Half-Orc", "Elf", "Half-Elf", "Dwarf", "halfling"]
var selectedClass, selectedRace;

let Main = {

  generateClassIcons: function () {
    document.getElementById("classDiv").innerHTML = '<h2 id="classSelectedDiv">Class:</h2>';
    document.getElementById("classDiv").innerHTML += '<div class="grid-container" id="classGrid"></div>';
    for (var i = 0; i < classT.length; i++) {
      classT[i]
      var classTLower = classT[i].toLowerCase();
      document.getElementById("classGrid").innerHTML += '<div class="classType" onclick="Main.popDetails(this.id, this.className);" id="' + classTLower + '">' + classT[i] + '<img src="./img/classes/' + classTLower + '.svg"></div>';
    }
    console.log("loaded Classes");
  },
  generateStatsView: function () {
    document.getElementById("statsDiv").innerHTML = '<h2 id="statsSelectedDiv">Stats:</h2>';
    document.getElementById("statsDiv").innerHTML += '<div class="grid-container" id="statsGrid"></div>';

    document.getElementById("statsGrid").innerHTML += '<div class="statsType"></div>';
    console.log("loaded view");
    let stat = [];
    for (var j = 0; j < 6; j++) {
      let nstat = Main.roll(4, 6);
      removeMin(nstat);
      var sum = nstat.reduce((a, b) => a + b, 0);
      stat.push(sum);
    }
    var totalStats = stat.reduce((a, b) => a + b, 0);
    if (totalStats < 70) {
      console.log("Regenerating stats because less than 70");
      Main.generateStatsView();
    } else {
      let getInterface = document.querySelector('.statsType');
      getInterface.innerHTML = '<p> ' + stat + ' total = ' + totalStats + '</p>'
      for (var i = 0; i < statList.length; i++) {
        getInterface.innerHTML += '<p>' + statList[i] + ' <select id="' + statAbr[i] + '" class="dropdown-stats"></select></p>'
        for (var sta in stat) {
          var abr = document.getElementById(statAbr[i]);
          abr.add(new Option(stat[sta]));
        };

      }
      document.getElementById("statsGrid").innerHTML += '<button class="btn-save btn btn-primary">SAVE</button>';
      $('.dropdown-stats').append('<option value=null selected="selected"></option>');
      $("select").change(function () {
        DisableOptions(); //disable selected values
      });
      function DisableOptions() {
        var arr = [];
        $("select option:selected").each(function () {
          arr.push($(this).index());
        });
        $("select option").filter(function () {
          return $.inArray($(this).index(), arr) > -1;
        }).attr("disabled", "disabled");
      }

    }
  },

  renderSectionThree: function () {

    var keys = Object.keys(localStorage)
    console.log(keys);
    let listEnemies = [];
    let listPlayers = [];
    for (let j = 0; j < keys.length; j++) {
      let itemsArray = localStorage.getItem(keys[j]) ? JSON.parse(localStorage.getItem(keys[j])) : [];
      console.log(itemsArray);
      if (itemsArray.charType == "npc") {
        console.log("npc detected");
        document.getElementById('enemy-char').innerHTML += '<p> '+ JSON.stringify(itemsArray, undefined, 2)+'';
        listEnemies.push(itemsArray);
        console.log(listEnemies);

      } else if (itemsArray.charType == "pc") {
        document.getElementById('player-char').innerHTML += '<p> '+ JSON.stringify(itemsArray, undefined, 2)+'';
        listPlayers.push(itemsArray);
        console.log(listPlayers);
      }

    }


  },



  generateRaceIcons: function () {
    document.getElementById("raceDiv").innerHTML = '<h2 id="raceSelectedDiv">Race:</h2>';
    document.getElementById("raceDiv").innerHTML += '<div class="grid-container" id="raceGrid"></div>';
    for (var i = 0; i < raceT.length; i++) {
      raceT[i]
      var raceTLower = raceT[i].toLowerCase();
      document.getElementById("raceGrid").innerHTML += '<div class="raceType" onclick="Main.popDetails(this.id, this.className);" id="' + raceTLower + '">' + raceT[i] + '<img src="./img/races/' + raceTLower + '.svg"></div>';
    }
    document.getElementById("raceGrid").innerHTML += '<button class="btn-save btn btn-primary">SAVE</button>';

    console.log("loaded Races");
  },
  //function to generate die rolls
  roll: function (times, die) {
    let dieArray = [];
    for (var j = 0; j < times; j++) {
      var n = Math.floor((Math.random() * die) + 1);
      dieArray.push(n);
    }
    return dieArray;
  },

  //this function get values for each race and class box created
  popDetails: function (s, a) {
    var sStatus = document.getElementById(s)
    var type;
    type = "." + a;
    $(type).css("height", "inherit");
    $(type).css("backgroundColor", "rgba(255, 255, 255, 0.8)"); $(type).css("boxShadow", "1px 2px 3px black");
    document.getElementById(s).style.backgroundColor = "white";
    document.getElementById(s).style.boxShadow = " inset 7px 7px 5px -4px rgba(0,0,0,0.75)";

    if (sStatus.status == "selected") {
      sStatus.status = "notselected";
      $(type).css("height", "inherit");

    } else if (sStatus.status == "notselected") {
      document.getElementById(s).style.height = "400px";
      sStatus.status = "selected";
    } else {
      document.getElementById(s).style.height = "400px";
      sStatus.status = "selected";
    }
    console.log(sStatus.status);

    if (a === "classType") {
      selectedClass = s.charAt(0).toUpperCase() + s.slice(1);
      document.getElementById("classSelectedDiv").innerHTML = '<h2>Class: ' + selectedClass + '</h2>';
      return selectedClass;

    } else if (a === "raceType") {
      selectedRace = s.charAt(0).toUpperCase() + s.slice(1);;
      document.getElementById("raceSelectedDiv").innerHTML = '<h2>Race: ' + selectedRace + '</h2>';
      return selectedRace;
    }
  },
}

Main.generateStatsView();
Main.generateClassIcons();
Main.generateRaceIcons();
Main.renderSectionThree();
//JQUERY CALLS FOR FUNCTION
$('#nxtBtn').click(function () {

  let playerTest = new Char("Hunter", "Human", "npc");
  playerTest.str = 10;
  playerTest.dex = 14;
  playerTest.getBonus();
  localStorage.setItem('playerTest', JSON.stringify(playerTest));
  let nonplayer = new Char("Barbarian", "Human", "npc");
  localStorage.setItem('nonplayer', JSON.stringify(nonplayer));
  window.location = '#section2';
  console.log(allStorage());






});


function allStorage() {
  var archive = [],
    keys = Object.keys(localStorage),
    i = 0, key;

  for (; key = keys[i]; i++) {
    archive.push(key + '=' + localStorage.getItem(key));
  }

  return archive;
}


$('.btn-save').click(function () {



  let str = $('#str option:selected').text();
  let dex = $('#dex option:selected').text();
  let con = $('#con option:selected').text();
  let int = $('#int option:selected').text();
  let wis = $('#wis option:selected').text();
  let cha = $('#cha option:selected').text();  //TO-DO check all passing variable
  if (str && dex && con && int && wis && cha) {
    if (selectedRace == null || selectedClass == null) {
      alert("Please select a class and a race");
    }
    let player = new Char(selectedClass, selectedRace, "pc", 200, str, dex, con, int, wis, cha);

    player.getBonus();
    localStorage.setItem('player', JSON.stringify(player));
    document.getElementById('section1').innerHTML = '<div class="box" id="savedDetails"><p></p></div>';
    document.getElementById('savedDetails').innerHTML = JSON.stringify(player, undefined, 2);
    window.location = '#section2';

  } else {
    alert("Please select the stats for character");
    console.log("empty values detected");
  }
});
function removeMin(arr) { //function to remove min value from an array
  var low = (Math.min.apply(null, arr));
  arr.splice(arr.indexOf(low), 1);
  return arr;
}

//
$('#game-ui').click(function () {
  var dValue = $('#enemy-char').css("display");
  if (dValue == "block") {
    $('#enemy-char').css("display", "none");
  } else {
    $('#enemy-char').css("display", "block");
  }
});

