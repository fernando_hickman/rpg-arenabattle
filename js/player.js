let char
function Char(classType, raceType, charType, health, str, dex, con, int, wis, cha) {
    this.classType = classType;
    this.raceType = raceType;
    this.charType = charType;
    this.health = health;
    this.str = str;
    this.dex = dex;
    this.con = con;
    this.int = int;
    this.wis = wis;
    this.cha = cha;
    this.getModifier = function () {
        switch (stat) {
            case (1):
                statModifier = -5
                break;
            case (2 || 3):
                statModifier = -4
                break;
            case (4 || 5):
                statModifier = -3
                break;

            case (6 || 7):
                statModifier = -2
                break;

            case (8 || 9):
                statModifier = -1
                break;

            case (10 || 11):
                statModifier = 0
                break;

            case (12 || 13):
                statModifier = +1
                break;

            case (14 || 15):
                statModifier = +2
                break;

            case (16 || 17):
                statModifier = +3
                break;

            case (18 || 19):
                statModifier = +4

                break;
            case (20 || 21):
                statModifier = +5

                break;
            case (22 || 23):
                statModifier = +6

                break;
            case (24 || 25):
                statModifier = +7

                break;
            case (26 || 27):
                statModifier = +8

                break;
            case (28 || 29):
                statModifier = +9

                break;
            case (30):
                statModifier = +10
                break;

        }


    }


    this.getBonus = function () {

        switch (classType) {
            case 'Fighter':
                this.str = Number(this.str) + 2;
                break;
            case 'Barbarian':
                this.str = Number(this.str) + 2;
                break;
            case 'Hunter':
                this.dex = Number(this.dex) + 2;
                break;
            case 'Rogue':
                this.dex = Number(this.dex) + 2;
                break;
            case 'Paladin':
                this.str = Number(this.str) + 2;
                break;
            case 'Sorcerer':
                this.int = Number(this.int) + 2;
                break;
            case 'Warlock':
                this.int = Number(this.int) + 2;
                break;
            case 'Wizard':
                this.int = Number(this.int) + 2;
                break;

        }

        switch (raceType) {
            case 'Human':
                this.str = Number(this.str) + 1;
                this.dex = Number(this.dex) + 1;
                this.con = Number(this.con) + 1;
                this.int = Number(this.int) + 1;
                this.wis = Number(this.wis) + 1;
                this.cha = Number(this.cha) + 1;
                break;
            case 'Half-orc':
                this.str = Number(this.str) + 2;
                this.con = Number(this.con) + 1;

                break;
            case 'Elf':
                this.dex = Number(this.dex) + 2;

                break;
            case 'Halfling':
                this.dex = Number(this.dex) + 2;

                break;
            case 'Half-elf':
                this.str = Number(this.str) + 1;
                this.dex = Number(this.dex) + 1;
                this.cha = Number(this.cha) + 2;
                break;
            case 'Dwarf':
                this.con = Number(this.con) + 2;

                break;
            case 'Dragonborn':
                this.str = Number(this.str) + 2;
                this.cha = Number(this.cha) + 1;
                break;
            case 'Gnome':
                this.int = Number(this.int) + 2;

                break;
        }
    }
}